package com.example.mike.calculadora;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Button one,two,three,four,five,six,seven,eight, nine,zero, dot,
            plus, minus, slash, sqrt,pow,times,clr, igual;
    TextView entrada;
    String res;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        entrada = (TextView) findViewById(R.id.textView2);

        one= (Button) findViewById(R.id.button);
        two= (Button) findViewById(R.id.button2);
        three= (Button) findViewById(R.id.button3);
        four= (Button) findViewById(R.id.button4);
        five= (Button) findViewById(R.id.button5);
        six= (Button) findViewById(R.id.button6);
        seven= (Button) findViewById(R.id.button7);
        eight = (Button) findViewById(R.id.button8);
        nine= (Button) findViewById(R.id.button9);
        zero = (Button) findViewById(R.id.button11);
        dot= (Button) findViewById(R.id.button10);
        plus= (Button) findViewById(R.id.button13);
        minus = (Button) findViewById(R.id.button14);
        slash = (Button) findViewById(R.id.button12);
        sqrt= (Button) findViewById(R.id.button22);
        pow = (Button) findViewById(R.id.button21);
        times = (Button) findViewById(R.id.button15);
        clr = (Button) findViewById(R.id.button24);
        igual = (Button) findViewById(R.id.button19);

        one.setOnClickListener((View.OnClickListener) this);
        two.setOnClickListener((View.OnClickListener) this);
        three.setOnClickListener((View.OnClickListener) this);
        four.setOnClickListener((View.OnClickListener) this);
        five.setOnClickListener((View.OnClickListener) this);
        six.setOnClickListener((View.OnClickListener) this);
        seven.setOnClickListener((View.OnClickListener) this);
        eight.setOnClickListener((View.OnClickListener) this);
        nine.setOnClickListener((View.OnClickListener) this);
        zero.setOnClickListener((View.OnClickListener) this);
        dot.setOnClickListener((View.OnClickListener) this);
        plus.setOnClickListener((View.OnClickListener) this);
        minus.setOnClickListener((View.OnClickListener) this);
        slash.setOnClickListener((View.OnClickListener) this);
        sqrt.setOnClickListener((View.OnClickListener) this);
        pow.setOnClickListener((View.OnClickListener) this);
        times.setOnClickListener((View.OnClickListener) this);
        clr.setOnClickListener((View.OnClickListener)this);
        igual.setOnClickListener((View.OnClickListener)this);

    }

    String vista = "0";
    String temp = "0";
    String op = "";
    String pot = "0";

    @Override
    public void onClick(View v) {
        if(vista == "0"){
            vista = "";
        }
        switch (v.getId()){
            case R.id.button:
                entrada.setText(vista+="1");
                break;
            case R.id.button2:
                entrada.setText(vista+="2");
                break;
            case R.id.button3:
                entrada.setText(vista+="3");
                break;
            case R.id.button4:
                entrada.setText(vista+="4");
                break;
            case R.id.button5:
                entrada.setText(vista+="5");
                break;
            case R.id.button6:
                entrada.setText(vista+="6");
                break;
            case R.id.button7:
                entrada.setText(vista+="7");
                break;
            case R.id.button8:
                entrada.setText(vista+="8");
                break;
            case R.id.button9:
                entrada.setText(vista+="9");
                break;
            case R.id.button11:
                entrada.setText(vista+="0");
                break;
            case R.id.button10:
                if(vista.charAt(vista.length()-1)!='.') {
                    entrada.setText(vista += ".");
                }
                break;
            case R.id.button24:
                vista = "0";
                temp = "0";
                break;
            case R.id.button13:
                if(vista != "") {
                    temp = vista;
                    vista = "0";
                    op = "suma";
                }else{
                    Toast toast1 = Toast.makeText(getApplicationContext(),
                            "Debe ingresar un numero antes para poder sumar",
                            Toast.LENGTH_SHORT);
                    toast1.show();
                }
                break;
            case R.id.button14:
                if(vista != "") {
                    temp = vista;
                    vista = "0";
                    op = "resta";
                }else{
                    Toast toast1 = Toast.makeText(getApplicationContext(),
                            "Debe ingresar un numero antes para poder restar",
                            Toast.LENGTH_SHORT);
                    toast1.show();
                }
                break;
            case R.id.button15:
                if(vista != "") {
                    temp = vista;
                    vista = "0";
                    op = "mult";
                }else{
                    Toast toast1 = Toast.makeText(getApplicationContext(),
                            "Debe ingresar un numero antes para poder multiplicar",
                            Toast.LENGTH_SHORT);
                    toast1.show();
                }
                break;
            case R.id.button12:
                if(vista != "") {
                    temp = vista;
                    vista = "0";
                    op = "div";
                }else{
                    Toast toast1 = Toast.makeText(getApplicationContext(),
                            "Debe ingresar un numero antes para poder dividir",
                            Toast.LENGTH_SHORT);
                    toast1.show();
                }
                break;
            case R.id.button21:
                if(vista != "") {
                    pot = vista;
                    vista += "^";
                    op = "pow";
                }else{
                    Toast toast1 = Toast.makeText(getApplicationContext(),
                            "Debe ingresar un numero antes para poder elevar a una potencia",
                            Toast.LENGTH_SHORT);
                    toast1.show();
                }
                break;
            case R.id.button23:
                vista = this.fact(Integer.parseInt(vista)) + "";
                break;
            case R.id.button22:
                vista = this.sqrt(vista);
                break;
            case R.id.button19:
                Double resp =0.0;
                if(vista == "" || temp == "")
                    return;
                switch (op){
                    case "suma":
                        resp = (Double.parseDouble(vista)+Double.parseDouble(temp));
                        break;
                    case "resta":
                        resp = (Double.parseDouble(vista)-Double.parseDouble(temp));
                        break;
                    case "mult":
                        resp = (Double.parseDouble(vista)*Double.parseDouble(temp));
                        break;
                    case "div":
                        resp = (Double.parseDouble(vista)/Double.parseDouble(temp));
                        if((resp+"").length()>=13)
                            resp = Double.parseDouble((resp+"").substring(0,12));
                        break;
                    case "pow":
                        String [] algo = this.separar(vista);
                        resp = this.powm(algo[0],algo[1]);
                }


                vista = resp+"";
                break;

        }

        entrada.setText(vista);

    }

    public String sqrt(String s){
        String resp =(Math.sqrt(Double.parseDouble(s))+"");
        if(resp.length()>=13){
            return resp.substring(0,12);
        }
        else{
            return resp;
        }
    }

    public Double powm(String s,String p){
        System.out.println("powm"+" s:"+s+ " p:"+p);
        return Math.pow(Double.parseDouble(s),Double.parseDouble(p));
    }

    public int fact(int s){
        System.out.println(s);
        return (s==0)?1:s*this.fact(s-1);
    }

    public String[] separar(String s){
        String a[] = new String [2];
        String temp = "";
        boolean cambio=false;
        for (int i = 0; i < s.length(); i++) {
            cambio = s.charAt(i)=='^';
            if(cambio){
                a[0]=temp;
                temp ="";
            }
            temp+= s.charAt(i);
        }
        a[1]=temp.substring(1,temp.length());
        return a;
    }
}
